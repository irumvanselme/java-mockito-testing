package com.okava.cart.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "items")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String title;

    private Integer quantity;

    private Float unitPrice;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "cart_id")
    private Cart cart;

    public Item(String title, Integer quantity, Float unitPrice) {
        this.title = title;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    public Item(String title, Integer quantity, Float unitPrice, Cart cart) {
        this.title = title;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.cart = cart;
    }

    public Float getTotalPrice() {
        return unitPrice * quantity;
    }
}
