package com.okava.cart.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "carts")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String customerNames;

    @OneToMany(mappedBy = "cart")
    private List<Item> items = new ArrayList<>();

    public Cart(String customerNames) {
        this.customerNames = customerNames;
    }

    public Cart(Long id, String customerNames) {
        this.id = id;
        this.customerNames = customerNames;
    }

    public Double getTotalAmount(){
        Double sum = 0.0;
        for (Item item: items)
            sum += item.getTotalPrice();

        return sum;
    }
}
