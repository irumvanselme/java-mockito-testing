package com.okava.cart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LastTestingApplication {

    public static void main(String[] args) {
        SpringApplication.run(LastTestingApplication.class, args);
    }

}
