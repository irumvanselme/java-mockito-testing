package com.okava.cart.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CreateCartDTO {

    @NotEmpty
    private String customerNames;
}
