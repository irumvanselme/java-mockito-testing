package com.okava.cart.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AddItemDTO {

    @NotNull
    private Long cartId;

    @NotEmpty
    private String title;

    @NotNull
    private Integer quantity;

    @NotNull
    private Float unitPrice;
}
