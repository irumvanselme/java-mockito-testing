package com.okava.cart.controllers;

import com.okava.cart.dtos.AddItemDTO;
import com.okava.cart.models.Item;
import com.okava.cart.services.IItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/cart-items")
public class ItemsController {

    private final IItemService itemService;

    @Autowired
    public ItemsController(IItemService itemService) {
        this.itemService = itemService;
    }

    @PostMapping
    public ResponseEntity<?> addToCart(@Valid @RequestBody AddItemDTO dto) {
        Item item = itemService.add(dto.getCartId(), dto.getTitle(), dto.getQuantity(), dto.getUnitPrice());
        System.out.println(item);
        return ResponseEntity.status(HttpStatus.CREATED).body(item);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> remove(@PathVariable Long id) {
        itemService.remove(id);
        return ResponseEntity.accepted().body("Removed");
    }
}
