package com.okava.cart.controllers;

import com.okava.cart.dtos.CreateCartDTO;
import com.okava.cart.services.ICartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/carts")
public class CartController {

    private final ICartService cartService;

    @Autowired
    public CartController(ICartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping("")
    public ResponseEntity<?> all(){
        return ResponseEntity.ok(cartService.all());
    }

    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody CreateCartDTO dto){
        return ResponseEntity.ok(cartService.create(dto.getCustomerNames()));
    }
}
