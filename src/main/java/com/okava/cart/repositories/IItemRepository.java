package com.okava.cart.repositories;

import com.okava.cart.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IItemRepository extends JpaRepository<Item, Long> {
    
}
