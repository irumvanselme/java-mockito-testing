package com.okava.cart.services;

import com.okava.cart.models.Cart;

import java.util.List;

public interface ICartService {

    List<Cart> all();

    Cart findById(Long id);

    Cart create(String customerNames);

    Cart save(Cart cart);
}
