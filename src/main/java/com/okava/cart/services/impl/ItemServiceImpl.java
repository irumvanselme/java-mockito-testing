package com.okava.cart.services.impl;

import com.okava.cart.models.Cart;
import com.okava.cart.models.Item;
import com.okava.cart.repositories.IItemRepository;
import com.okava.cart.services.ICartService;
import com.okava.cart.services.IItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemServiceImpl implements IItemService {

    private final IItemRepository itemRepository;

    private final ICartService cartService;

    @Autowired
    public ItemServiceImpl(IItemRepository itemRepository, ICartService cartService) {
        this.itemRepository = itemRepository;
        this.cartService = cartService;
    }

    @Override
    public Item findById(Long id) {
        return itemRepository.findById(id).orElseThrow(() -> new RuntimeException("Cart item with " + id + " not found"));
    }

    @Override
    public Item add(Long cartId, String title, Integer quantity, Float unitPrice) {
        Cart cart = cartService.findById(cartId);
        return create(cart, title, quantity, unitPrice);
    }

    @Override
    public boolean remove(Long itemId) {
        Item item = findById(itemId);

        itemRepository.delete(item);

        return true;
    }

    @Override
    public Item create(Cart cart, String title, Integer quantity, Float unitPrice){
        return itemRepository.save(new Item(title, quantity, unitPrice, cart));
    }
}
