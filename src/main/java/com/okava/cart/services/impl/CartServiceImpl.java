package com.okava.cart.services.impl;

import com.okava.cart.models.Cart;
import com.okava.cart.repositories.ICartRepository;
import com.okava.cart.services.ICartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartServiceImpl implements ICartService {

    private final ICartRepository cartRepository;

    @Autowired
    public CartServiceImpl(ICartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Override
    public List<Cart> all() {
        return cartRepository.findAll();
    }

    @Override
    public Cart findById(Long id) {
        return cartRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Cart with " + id + " not found"));
    }

    @Override
    public Cart create(String customerNames) {
        Cart cart = new Cart(customerNames);
        return cartRepository.save(cart);
    }

    @Override
    public Cart save(Cart cart) {
        return cartRepository.save(cart);
    }
}
