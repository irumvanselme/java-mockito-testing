package com.okava.cart.services;

import com.okava.cart.models.Cart;
import com.okava.cart.models.Item;

public interface IItemService {

    Item findById(Long id);

    Item add(Long cartId, String title, Integer quantity, Float unitPrice);

    boolean remove(Long item);

    Item create(Cart cart, String title, Integer quantity, Float unitPrice);
}
