package com.okava.cart.services;

import com.okava.cart.models.Cart;
import com.okava.cart.models.Item;
import com.okava.cart.repositories.ICartRepository;
import com.okava.cart.services.impl.CartServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CartServiceTests {

    @Mock
    private ICartRepository cartRepository;

    @InjectMocks
    private CartServiceImpl cartService;

    @Test
    public void testGetAllCarts() {
        when(cartRepository.findAll()).thenReturn(Arrays.asList(new Cart("John"), new Cart("Mark")));

        assertEquals("John", cartService.all().get(0).getCustomerNames());
    }

    @Test
    public void testFindById() {
        when(cartRepository.findById(anyLong())).thenReturn(Optional.of(new Cart("John")));

        assertEquals("John", cartService.findById(1L).getCustomerNames());
    }

    @Test
    public void testCreateCart() {
        when(cartRepository.save(any())).thenReturn(new Cart("John"));

        assertEquals("John", cartService.create("John").getCustomerNames());
    }

    @Test
    public void testOnCreateCartThatTotalPriceIsZero() {
        when(cartRepository.save(any())).thenReturn(new Cart("John"));

        Cart newCart = cartService.create("John");

        assertEquals(Double.valueOf(0), newCart.getTotalAmount());
    }

    @Test
    public void testOnCreateCartThatTotalItemsIsZero() {
        when(cartRepository.save(any())).thenReturn(new Cart("John"));

        Cart newCart = cartService.create("John");

        assertEquals(0, newCart.getItems().size());
    }

    @Test
    public void testOnAddToCartThatTotalItemsIsIncremented() {
        when(cartRepository.findById(anyLong())).thenReturn(Optional.of(new Cart(100L, "John")));

        Cart cart = cartService.findById(1L);
        cart.getItems().add(new Item("Sugar", 1, 1000F)); // add items
        cart.getItems().add(new Item("Salt", 3, 2000F)); // add items

        when(cartRepository.save(any())).thenReturn(cart);

        Cart theUpdatedCart = cartService.save(cart);

        assertEquals(2, theUpdatedCart.getItems().size()); // I expect to get the two items
    }

    @Test
    public void testOnRemoveFromCartThatTotalItemsIsReduced() {
        when(cartRepository.findById(anyLong())).thenReturn(Optional.of(new Cart(100L, "John")));

        Cart cart = cartService.findById(1L);
        cart.getItems().add(new Item("Sugar", 1, 1000F)); // add items
        cart.getItems().add(new Item("Salt", 3, 2000F)); // add items

        // now we have 2 items in the cart

        cart.getItems().remove(1); // I am going to remove one

        when(cartRepository.save(any())).thenReturn(cart);

        Cart theUpdatedCart = cartService.save(cart);

        assertEquals(1, theUpdatedCart.getItems().size()); // I expect to get the (2 - 1) = 1 item in the cart
    }
}
