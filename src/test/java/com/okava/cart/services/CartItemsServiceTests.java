package com.okava.cart.services;

import com.okava.cart.models.Cart;
import com.okava.cart.models.Item;
import com.okava.cart.repositories.IItemRepository;
import com.okava.cart.services.impl.ItemServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CartItemsServiceTests {

    @Mock
    private IItemRepository iItemRepository;

    @Mock
    private ICartService cartService;

    @InjectMocks
    private ItemServiceImpl itemService;

    @Test
    public void testFindById() {
        when(iItemRepository.findById(anyLong())).thenReturn(Optional.of(new Item("Sugar", 1, 1000F, null)));

        assertEquals("Sugar", itemService.findById(1L).getTitle());
    }

    @Test
    public void testAddToCart() {
        when(iItemRepository.save(any())).thenReturn(new Item("Sugar", 1, 1000F, null));
        when(cartService.findById(anyLong())).thenReturn(new Cart("John"));

        assertEquals("Sugar", itemService.add(1L, "Sugar", 1, 1000F).getTitle());
    }

    @Test
    public void testRemoveFromCat() {
        when(iItemRepository.findById(anyLong())).thenReturn(Optional.of(new Item("Sugar", 1, 1000F)));

        boolean results = itemService.remove(1L);

        assertTrue(results);
    }
}
